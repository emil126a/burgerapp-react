import React, {Component} from 'react'
import Burger from './../../components/Burger/Burger'
import BuildControls from './../../components/Burger/BuildControls/BuildControls'
import Auxi from '../../hoc/Auxi/Auxi'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummmary from '../../components/Burger/OrderSummary/OrderSummary'
import axios from '../../axios-orders'
import Spinner from '../../components/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'

const INGREDIENT_PRICE = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
}


class BurgerBuilder extends Component {

    state = {
        ingredients: null,
        totalPrice: 4,
        purchaseable: false,
        purchasing: false,
        loading: false,
        error: false
    }


    purchaseHandler = () => {
        this.setState({purchasing: true})
    }

    purchaseCancelHandler = () => {
        this.setState({purchasing: false})
    }

    purchaseContinueHandler = () => {
        this.setState({loading: true})
        const orders = {
            ingredients: this.state.ingredients,
            price: this.state.totalPrice,
            customer: {
                name: 'Max Schwarzmuller',
                adress: {
                    street: 'TestS1treet 1',
                    zipCode: '41351',
                    country: 'Germany'
                },
                email: 'test@test.com'
            },
            deliveryMethod: 'fastest'
        }
        axios.post('https://react-my-burger-f436f.firebaseio.com/orders.json', orders)
            .then(response => {
                this.setState({loading: false, purchasing: false})
            })
            .catch(error => {
                this.setState({loading: false, purchasing: false})
            })
    }


    updatePurchaseState(ingredients) {
        const sum = Object.keys(ingredients).map(igKey => {
            return ingredients[igKey];
        }).reduce((sum, el) => {
            return sum + el //ingredients objectini gezib value-larin cemini tapir ve bir value olaraq return edir.
        }, 0);

        this.setState({purchaseable: sum > 0})
    }


    addIngredientHandler = (type) => {//Type= ingredient type
        const oldCount = this.state.ingredients[type];

        const updatedCount = oldCount + 1
        const updatedIngredients = {//updete uchun ele bil butun objecti goturub havada saxlayir. Yani birbasha mudaxile etmir. immutable update
            ...this.state.ingredients
        }
        updatedIngredients[type] = updatedCount// sonra update edir state-i
        const priceAddition = INGREDIENT_PRICE[type]
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;
        this.setState({totalPrice: newPrice, ingredients: updatedIngredients})
        this.updatePurchaseState(updatedIngredients)
    }

    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];

        if (oldCount <= 0) {//Eger more button click olunmayibsa onda hech ne etme.
            return;
        }
        const updatedCount = oldCount - 1
        const updatedIngredients = {//updete uchun ele bil butun objecti goturub havada saxlayir. Yani birbasha mudaxile etmir. immutable update
            ...this.state.ingredients
        }
        updatedIngredients[type] = updatedCount// sonra update edir state-i
        const priceDeduction = INGREDIENT_PRICE[type]
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceDeduction;
        this.setState({totalPrice: newPrice, ingredients: updatedIngredients})
        this.updatePurchaseState(updatedIngredients)
    }

    componentDidMount() {

        axios.get("https://react-my-burger-f436f.firebaseio.com/inredients.json").then(response => {
            this.setState({
                ingredients: response.data
            })

        }).catch(error => {
            this.setState({error: true})
        })
    }

    render() {
        const disabledInfo = {
            ...this.state.ingredients
        }

        for (let key in disabledInfo) {// Less Button disable etmek uchun

            disabledInfo[key] = disabledInfo[key] <= 0
            /* BIZE BELE NETICE QAYTARACAQ
            {
                salad:true,
                meat:false,
            }
            */
        }


        let orderSummary = null

        let burger = this.state.error ? <p>Ingredients can't be load</p> : <Spinner/> //Eger error varsa mesaj goster yoxsa spinner yukleyirik

        if (this.state.ingredients) {//ingredient null olmazsa. Yani yuklenib bitirse
            burger = (
                <Auxi>
                    <Burger ingredients={this.state.ingredients}/>
                    <BuildControls
                        ingredientAdded={this.addIngredientHandler}
                        ingredientRemoved={this.removeIngredientHandler}
                        disabled={disabledInfo}
                        purchaseable={this.state.purchaseable}
                        price={this.state.totalPrice}
                        ordered={this.purchaseHandler}
                    />
                </Auxi>
            )

            orderSummary = <OrderSummmary
                ingredients={this.state.ingredients}
                purchaseCanceled={this.purchaseCancelHandler}
                purchaseContinued={this.purchaseContinueHandler}
                price={this.state.totalPrice}
            />

            if (this.state.loading) {
                orderSummary = <Spinner/>
            }
        }


        return (

            <Auxi>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>

                {burger}
            </Auxi>

        );
    }

}

export default withErrorHandler(BurgerBuilder, axios)