import React, {Component} from 'react';
import Layout from './hoc/Layout/Layout'// import Layout - Burada boyuk herfle yazilan Layout faylin adidir
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder'

class App extends Component {
    render() {
        return (
            <div>

                <Layout>
                    <BurgerBuilder/>
                </Layout>

            </div>
        );
    }
}

export default App;