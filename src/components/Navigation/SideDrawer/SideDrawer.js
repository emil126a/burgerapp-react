import React from 'react'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import classes from './SideDrawer.css'
import Backdrop from './../../UI/Backdrop/Backdrop'
import Auxi from '../../../hoc/Auxi/Auxi'

const sideDrawer = (props) => {
    let attachedClasses = [classes.SideDrawer, classes.Close] /*.SideDrawer - her zaman, .Close - ise default olaraq gosterilecek*/

    if (props.open) { //Eger true olarsa
        attachedClasses = [classes.SideDrawer, classes.Open] /*.SideDrawer ve .Open class-larini gotur arraya bas  */
    }

    return (
        <Auxi>
            <Backdrop
                show={props.open}
                clicked={props.closed} /*Backdrop uzerine click etdikde close edeceyik.*/
            />

            <div className={attachedClasses.join(' ')}>
                <div className={classes.Logo}>
                    <Logo/>
                </div>

                <nav>
                    <NavigationItems/>
                </nav>
            </div>
        </Auxi>
    )
}


export default sideDrawer