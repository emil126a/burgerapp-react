import React from 'react'
import NavigationItem from './NavigationItem/NavigationItem'
import classes from './NavigationItems.css'

const navigationItems = (props) => (
    <ul className={classes.NavigationItems}>
        <NavigationItem active>Burger Builder</NavigationItem>{/*active true oldugunu bele de gondere bilerik*/}
        <NavigationItem>Checkout</NavigationItem>

    </ul>
)

export default navigationItems