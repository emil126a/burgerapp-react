import React from 'react'
import classes from './Button.css'


const button = (props) => (
    <button
        onClick={props.clicked}
        className={[classes.Button, classes[props.btnType]].join(' ')}//Choxlu class name-leri eyni elemente elave etmek uchun join ishletdik
    >{props.children}</button>
);

export default button;