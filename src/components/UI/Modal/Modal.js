import React, {Component} from 'react'
import classes from './Modal.css'
import Auxi from '../../../hoc/Auxi/Auxi'
import Backdrop from '../Backdrop/Backdrop'

class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState) { //Eger show parametri deyisherse rendere icaze ver
        if (nextProps.show !== this.props.show || nextProps.chilren !== this.props.children) {
            return true;
        }
    }

    render() {
        return (
            <Auxi> {/*Birden artiq component ishledeceyimize gore bu yalanci tagdan istifade edirik*/}
                <Backdrop show={this.props.show} clicked={this.props.modalClosed}/>
                <div
                    className={classes.Modal}
                    style={{
                        transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',//animation:Eger button click olunubsa slide down ele eks halda slide up
                        opacity: this.props.show ? '1' : '0'
                    }}
                >
                    {this.props.children}

                </div>
            </Auxi>
        )
    }
}


export default Modal