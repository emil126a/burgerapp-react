import React from 'react'
import classes from './Burger.css'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'

const burger = (props) => {
    let transformedInredients = Object.keys(props.ingredients)
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey}/>;

            });
        }).reduce((arr, el) => {
            return arr.concat(el) //bir array-e birleshdir
        }, [])//Yani array ichinde olan bosh array-lari gez

    if (transformedInredients.length === 0) {
        transformedInredients = <p>Please start adding ingredients!</p>
    }

    return (
        <div className={classes.Burger}>
            <BurgerIngredient type={'bread-top'}/>
            {transformedInredients}
            <BurgerIngredient type={'bread-bottom'}/>
        </div>
    )
}

export default burger;