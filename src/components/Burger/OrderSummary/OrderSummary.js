import React, {Component} from 'react'
import Auxi from '../../../hoc/Auxi/Auxi'
import Button from "../../UI/Button/Button"

class OrderSummary extends Component {


    render() {
        const ingredientSummary = Object.keys(this.props.ingredients)//Objecti arraya chevirir
            .map(
                igKey => {//igKey  = Salad
                    return (
                        <li key={igKey}>
                            <span style={{textTransform: 'capitalize'}}>{igKey}</span>:{this.props.ingredients[igKey]}
                        </li>
                    )
                }
            )
        return (
            <Auxi>
                <h3> Your Order </h3>
                <p>A delicious burger with the following ingredients:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p>Continue to Checkout?</p>

                <p>Total Price: <b>{this.props.price.toFixed(2)}</b></p>

                <Button btnType="Danger" clicked={this.props.purchaseCanceled}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued}>CONTINUE</Button>
            </Auxi>);
    }

}


export default OrderSummary