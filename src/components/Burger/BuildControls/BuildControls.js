import React from 'react'
import classes from './BuildControls.css'
import BuildControl from './BuildControl/BuildControl.js'
import Auxi from "../../../containers/BurgerBuilder/BurgerBuilder";


const controls = [//Tesevvur edek ki bizim burger bu ingredientlerden ibaret olacaq
    {label: 'Salad', type: 'salad'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Cheese', type: 'cheese'},
    {label: 'Meat', type: 'meat'},
]
const buildControls = (props) => (
    <div className={classes.BuildControls}>
        <p>Current Price: <b>{props.price.toFixed(2)}</b></p>
        {controls.map(ctrl => (//Array ichinde her bir elementi (ctrl) gezib onu diger componente pass edir
            <BuildControl
                key={controls.label}
                type={ctrl.type}
                label={ctrl.label}
                added={() => props.ingredientAdded(ctrl.type)}
                removed={() => props.ingredientRemoved(ctrl.type)}
                disabled={props.disabled[ctrl.type]}
            />

        ))}

        <button
            className={classes.OrderButton}
            disabled={!props.purchaseable}
            onClick={props.ordered}>
            ORDER NOW
        </button>
    </div>
)

export default buildControls
