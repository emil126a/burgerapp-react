import axios from 'axios'

const instance = axios.create({
    baseUrl: 'https://react-my-burger-f436f.firebaseio.com'
});

export default instance