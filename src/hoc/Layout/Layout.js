import React, {Component} from 'react'

import Auxi from '../Auxi/Auxi' //Auxi componentimizi import edek
import classes from './Layout.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer'


class Layout extends Component {


    state = {
        showSideDrawer: false
    }

    sideDrawerClosedHandler = () => /*Hem sidedrawer, hem de backdrop click edende bu funksiya trigger olunacaq*/ {
        this.setState({showSideDrawer: false})
    }


    drawerToggleClicked = () => {
        this.setState((prevState) => {
            return {
                showSideDrawer: !prevState.showSideDrawer
            }

        })
    }

    render() {
        return (
            <Auxi>
                <Toolbar drawerToggleClicked={this.drawerToggleClicked}/>
                <SideDrawer
                    closed={this.sideDrawerClosedHandler}
                    open={this.state.showSideDrawer}
                />
                <main className={classes.Content}>
                    {this.props.children}
                </main>

            </Auxi>)
    }
}

export default Layout;